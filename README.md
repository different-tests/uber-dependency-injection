# Uber Dependency Injection



## Getting started

Fx is a dependency injection system for Go.

 - reduce boilerplate in setting up your application
 - eliminate global state in your application
 - add new components and have them instantly accessible across the application
 - build general purpose shareable modules that just work

